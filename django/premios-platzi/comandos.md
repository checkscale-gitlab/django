Activar entorno virtual

```sh
pyenv activate premios-platzi
```

Salir del entorno virutal

```sh
pyenv deactivate
```

Levantar el servidor

```sh
python manage.py runserver
```

Migraciones de los cambios en los archivos.

```sh
python manage.py makemigrations polls
```

Migrar los cambios

```shell
python manage.py migrate
```

Shell Django

```sh
python manage.py shell
```