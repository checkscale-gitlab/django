<h1>Django</h1>

<h2>Facundo </h2>

<h1>Table of Contents</h1>

- [1. Introducción](#1-introducción)
  - [Bienvenido al desarrollo backend con Python](#bienvenido-al-desarrollo-backend-con-python)
  - [¿Qué es Django?](#qué-es-django)
- [2. Comenzando a programar en Django](#2-comenzando-a-programar-en-django)
  - [Instalación de Django](#instalación-de-django)
  - [Explorando los archivos que creó Django](#explorando-los-archivos-que-creó-django)
  - [El servidor de desarrollo](#el-servidor-de-desarrollo)
  - [Nuestro primer proyecto: Premios Platzi App](#nuestro-primer-proyecto-premios-platzi-app)
  - [Nuestro primer proyecto: Premios Platzi App 2](#nuestro-primer-proyecto-premios-platzi-app-2)
  - [Ajustando el archivo settings.py](#ajustando-el-archivo-settingspy)
- [3. Models](#3-models)
  - [¿Qué es ORM? ¿Qué es un modelo?](#qué-es-orm-qué-es-un-modelo)
  - [Creando un diagrama entidad-relación para nuestro proyecto](#creando-un-diagrama-entidad-relación-para-nuestro-proyecto)
  - [Creando los modelos Question y Choice](#creando-los-modelos-question-y-choice)
- [4. Interactive Shell](#4-interactive-shell)
  - [La consola interactiva de Django](#la-consola-interactiva-de-django)
  - [El método __str__](#el-método-str)
  - [Filtrando los objetos creados desde la consola interactiva](#filtrando-los-objetos-creados-desde-la-consola-interactiva)
  - [El método filter](#el-método-filter)
  - [Accediendo al conjunto de respuestas](#accediendo-al-conjunto-de-respuestas)
- [5. Django Admin](#5-django-admin)
  - [El administrador de Django](#el-administrador-de-django)
- [6. Views](#6-views)
  - [¿Qué son las views o vistas?](#qué-son-las-views-o-vistas)
  - [Creando vistas para la aplicación](#creando-vistas-para-la-aplicación)
  - [Templates de Django](#templates-de-django)
  - [Creando el template del home](#creando-el-template-del-home)
  - [Elevando el error 404](#elevando-el-error-404)
  - [Utilizando la etiqueta url para evitar el hard coding](#utilizando-la-etiqueta-url-para-evitar-el-hard-coding)
- [7. Forms](#7-forms)
  - [Formularios: lo básico](#formularios-lo-básico)
  - [Creando la vista vote](#creando-la-vista-vote)
  - [Creando la vista results](#creando-la-vista-results)
- [8. Generic Views](#8-generic-views)
  - [Generic Views](#generic-views)
  - [Implementando generic views en la aplicación](#implementando-generic-views-en-la-aplicación)
- [9.Conclusiones](#9conclusiones)
  - [Aprendiste mucho, pero, ¿estás listo para pasar al siguiente nivel?](#aprendiste-mucho-pero-estás-listo-para-pasar-al-siguiente-nivel)

# 1. Introducción

## Bienvenido al desarrollo backend con Python

![django.png](https://static.platzi.com/media/user_upload/django-8436fb25-37cd-4a0b-abf9-aa348bb8e85b.jpg)

## ¿Qué es Django?

![img](https://rodgithub.github.io/1.QueEsDjango.png)

**Django** es un framework, conjunto de reglas y código creado por terceros que te sirve para crear app webs.

- Aplicaciones importantes que usan django:
  Instagram, Pinterest, NATGEO, **Platzi**
- Django es grati y Open Source
- Framerwork veloz, es seguro y escalable (Pueda crecer de manera sencilla)
- Es el segundo framework en el top3.

> A continuación os presento algunas webs mundialmente conocidas que seguramente no sabíais que estaban construidas usando el framework Django
>
> - The New York Times
> - Nasa Science
> - Disqus
> - Mercedes-Benz

**🔒 Está es la protección que ofrece Django 🤠**

Protección contra la falsificación de solicitudes en sitios cruzados (CSRF):

Es la mencionada por el profe. Los ataques CSRF permiten que un usuario malintencionado ejecute acciones utilizando las credenciales de otro usuario sin el conocimiento de este. 👿

Django tiene protección incorporada contra la mayoría de los tipos de ataques CSRF. 😎

Protección de inyección SQL:

La inyección SQL es un tipo de ataque en el que un usuario malicioso es capaz de ejecutar código SQL arbitrario en una base de datos. 💉

Los conjuntos de consultas de Django están protegidos de la inyección de SQL ya que sus consultas se construyen utilizando la parametrización de consultas.

🖱 Protección contra el clickjacking:
El clickjacking es un tipo de ataque en el que un sitio malicioso envuelve a otro sitio en un marco. Este ataque puede hacer que un usuario desprevenido sea engañado para que realice acciones no deseadas en el sitio objetivo.

Django contiene protección contra clics en forma de X-Frame-Options middleware que en un navegador compatible puede evitar que un sitio se represente dentro de un marco.

❌ Protección de la escritura de sitios cruzados (XSS):

Los ataques XSS permiten a un usuario inyectar scripts del lado del cliente en los navegadores de otros usuarios. 💉

El uso de plantillas Django te protege contra la mayoría de los ataques XSS. 😎

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Django vs. Flask vs. FastAPI](https://platzi.com/blog/django-flask-fastapi/)

# 2. Comenzando a programar en Django

## Instalación de Django

Crear entorno virutal

```sh
pyenv virtualenv premios-platzi
```

Activar pyenv

```sh
pyenv activate premios-platzi
```

Desactivar pyenv

```sh
pyenv deactivate
```

Instalar django

```sh
pip install django
```

Crear proyecto

```shell
➜ django-admin startproject premiosplatziapp
```

> En este [enlace](https://www.toptal.com/developers/gitignore) os dejo un sitio muy recomendado para crear los `.gitignore` que necesites, en este caso, sólo escribe Django en la caja de texto y te va a generar el `.gitignore` dedicado para este framework.

[![img](https://www.google.com/s2/favicons?domain=https://static.djangoproject.com/img/icon-touch.e4872c4da341.png)Django documentation | Django documentation | Django](https://docs.djangoproject.com/en/3.2/)

## Explorando los archivos que creó Django

![Django Folder Structure](https://studygyaan.com/wp-content/uploads/2019/07/Best-Practice-to-Structure-Django-Project-Directories-and-Files.png)

![django 2022-01-20 000754.jpg](https://static.platzi.com/media/user_upload/django%202022-01-20%20000754-c598777a-306b-4aa3-8305-167f46bbc07f.jpg)

> Este aporte lo hizo un Platzinauta llamado **Cesar David Ramírez Dimaté** en el curso de Django 2018.

Lo tenía guardado porque ayuda mucho cundo estas empezando a aprender el framework.

premiosplatziapp: carpeta contenedora del proyecto, todos los archivos necesarios para que la app funcione (Contiene los archivos sin vinculación a djnago)

manage-py , sirve para trabajar con los comandos disponibles

premiosplatziapp, esta segunda carpeta si esta afectada por django.

- **init**-py : Nos dice que la carpeta es un paquete.
- asgi-py & wsgi-py : Nos ayudan con el deploy.
- settings-py : Contiene toda la configuración y constantes del proyecto.
- url-py : Las direcciones para movernos en el proyecto.

## El servidor de desarrollo

Siempre que crees un proyecto web (Siendo uno mismo el creador) siempre va “vivir” en dos lugares:

- En local: Entorno de trabajo que creamos y editamos para desarrollar.
- En producción, es el servidor (Jamas tocamos el código directamente).
  Django nos permite utilizar un servidor (local) de desarrollo.
  Para ver el servidor local debemos ejecutar el comando: `py manage-py runserver`

Detalle de mensaje:

- **Watching for file … :** A cada cambio que hagas en los archivo, Django lo notará y lo reflejará en el servidor.
- **System check identified no issues … :** No hay problemas y se silenciaron 0.
- **You have 18 unapplied migration(s) … :** No se ha creado una base de datos efectiva.
- **Date, Version Django**
- **Using settings ‘premiosplatziapp.settings’ :** Tomar el archivo settings-py, ver la variables de configuración y aplicarlas para tenerlas disponibles al crear el código.
- **Starting development … :** Servidor desplegado (iniciado) de manera local.

Correr el servidor

```sh
python manage.py runserver
```

## Nuestro primer proyecto: Premios Platzi App

Dentro de Django hay 2 cosas importantes para diferenciar:

- Proyecto : Un proyecto es una colección de configuraciones y aplicaciones para un sitio web en particular. Un proyecto puede contener varias aplicaciones. *Una aplicación puede estar en varios proyectos.*
- Apps : Una aplicación es una aplicación web que hace algo, por ejemplo, un sistema de blogs, una base de datos de registros públicos o una pequeña aplicación de encuestas.

Cada aplicación que escribe en Django consta de un paquete de Python que sigue una determinada convención. Django viene con una utilidad que genera automáticamente la estructura básica de directorios de una aplicación, por lo que puede concentrarse en escribir código en lugar de crear directorios.

> Una aplicación en Django es un conjunto portable de una **funcionalidad** de Django que típicamente incluye modelos y vistas, que conviven en un solo paquete de Python.

![Django-file-Structure.jpg](https://static.platzi.com/media/user_upload/Django-file-Structure-46ebc589-0736-4b81-8cba-e97348f2206c.jpg)

> Poniendo un ejemplo de como se podría dividir un sistema, el proyecto podría ser biblioteca, y las aplicaciones pudieran ser:
>
> 1. usuarios (manejo de usuarios, login, cambiar o recordar contraseñas, etc)
> 2. libros (altas, bajas, clasificación, ubicación física)
> 3. préstamos y devoluciones

[![img](https://www.google.com/s2/favicons?domain=https://static.djangoproject.com/img/icon-touch.e4872c4da341.png)Writing your first Django app, part 1 | Django documentation | Django](https://docs.djangoproject.com/en/3.2/intro/tutorial01/)

## Nuestro primer proyecto: Premios Platzi App 2

Crear app

```sh
python manage.py startapp polls
```



> **admin.site.urls** es la única excepción a esto.
>
> ¿Cuándo utilizar **include()** ?
>
> Siempre debe usar **include()** cuando incluye otros patrones de URL. **admin.site.urls** es la única excepción a esto.
>
> **premiosplatziapp/urls.py**
>
> ```python
> from django.contrib importadmin
> from django.urls importinclude, path
> 
> urlpatterns = [
>     path('polls/', include('polls.urls')),
>     path('admin/', admin.site.urls),    #Excepto de include()
> ]
> ```

**Archivos urls(.)py**
Sirven para definir cada una de las URLs que tendrá nuestro sitio web.

El archivo url(.)py está presente en la carpeta del proyecto (Paquete principal) y en cada aplicación, y estos tienen funcionalidades diferentes pero trabajan en conjunto.

El archivo url(.)py de la **carpeta del proyecto**, debe contener un include de cada archivo url(.)py de cada aplicación. Ejemplo:

```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('polls/', include('polls.urls'))
]
```

> Como se puede ver, se está haciendo un include de polls.urls, el cual es el archivo de urls(.)py de la aplicación polls.

El archivo url(.)py de cada **aplicación**, debe contener un listado de todas las URLs que usará su respectiva aplicación y cada una debe invocar a una función especifica que manejará toda la lógica lo relacionada a dicha URL(página). Estas funciones se ubicarán en el archivo views(.)py, archivo que tendrá toda la lógica de la aplicación (controlador), ejemplo:

```python
urlpatterns = [
    path('', views.index, name='index')
]
```

> views.index, views es el archivo views(.)py e index es la función a invocar.

Cada vez que se crea una **aplicación** se entiende que este tendrá sus propias URLs.

> **Ejemplo**, si creo una aplicación “encuetas” necesitaré una ruta “encueta/” o una ruta “encuesta/resultados”.

## Ajustando el archivo settings.py

En mi caso me llamó la atención la última linea.

```python
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
```

Según la documentación, en esta Setting podemos cambiar el tipo de **Primary Key** que tendrán por default los **Modelos** que creemos, cuando no definamos explicita-mente la propiedad **primary_key** en algún **atributo** de nuestro Modelo.

```python
id = models.BigAutoField(primary_key=True)
```

**BigAutoField** refiere a un número entero de 64-bits que incrementa automáticamente y va del **1** al **9223372036854775807**

> STATIC_URL, indica que en la carpeta de nombre static se van a encontrar todos esos archivos adicionales al proyecto, como imágenes, javascript files and css files. No viene creada por default por lo que es necesario crearla por nosotros y bajo ese nombre (static) por recomendación de Django.

[![img](https://www.google.com/s2/favicons?domain=https://en.wikipedia.org/wiki/List_of_tz_database_time_zones/static/apple-touch/wikipedia.png)List of tz database time zones - Wikipedia](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)



[middleware-django](https://pywombat.com/articles/middleware-django)

[middleware-django](https://docs.djangoproject.com/en/4.0/topics/http/middleware/)

# 3 Models

## ¿Qué es ORM? ¿Qué es un modelo?

Un **ORM (Object-Relational Mapping)** es una técnica que nos permite crear una **Base de Datos Orientada a Objectos (virtual)**, que opera sobre la **Base de Datos Relacional (real)**.

Utilizando un ORM podemos operar sobre la base de datos aprovechando las características propias de la orientación a objetos, como **herencia y polimorfismo**.

También podemos acceder a los atributos de una Entidad de la misma forma que accedemos a los atributos de una Clase, realizar operaciones para **obtener, crear, modificar y eliminar datos, todo desde el código de programación sin tener que escribir SQL**. Esto además nos permite escribir el código una sola vez y garantizarnos que va a seguir funcionando **incluso si en el futuro se cambia el motor de Base de Datos** (por ejemplo, de MySQL a Microsoft SQL Server).

![ORM-Django2.jpg](https://static.platzi.com/media/user_upload/ORM-Django2-939243d7-495f-4245-9c02-3eaab29d3367.jpg)

![ORM-Django.png](https://static.platzi.com/media/user_upload/ORM-Django-06d11474-5d63-4f1b-85b4-a032b46b9f0e.jpg)

![orm.png](https://static.platzi.com/media/user_upload/orm-34a8ef19-6baf-491f-b802-c8d119b6106e.jpg)

***ORM***
**Tablas** -> Modelos expresados mediante clases1
**Columnas** -> Atributos1 de las clases1
**Tipos de datos** -> Clases ligasdas a los Atributos1

> **(Object Relational Mapping)**
>
> Técnica que nos permite a través de ciertas librerías y frameworks relacionar la estructura de una Base de Datos Relacional (RDB) Relational Data Base, con la Programación Orientada a Objetos (Poo), es decir, manipular una base de datos a través objetos ubicados en un archivo “.py”, ya que en este archivo hay una representación las tablas de la base de datos.

[Funciones extras del ORM de Django](https://platzi.com/blog/funciones-extras-de-django-orm/?gclid=Cj0KCQiAubmPBhCyARIsAJWNpiMXtwlLaiIeBEXbvWkhAkqlebQAkwwsgVCSCnqhHaflelCJUHVygocaAsePEALw_wcB&gclsrc=aw.ds)

## Creando un diagrama entidad-relación para nuestro proyecto

![Untitled (1).png](https://static.platzi.com/media/user_upload/Untitled%20%281%29-1d6350c3-b6d5-4752-ae16-2bd4d9210376.jpg)

![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-89202151-e764-47b1-8cfd-7270cbf34850.jpg)

>  Tabla -> Polls![Screen Shot 2022-02-19 at 12.00.36.png](https://static.platzi.com/media/user_upload/Screen%20Shot%202022-02-19%20at%2012.00.36-ccc62efc-4d7b-484d-a5e3-c89aeaaaf3c9.jpg)

[Normalizacon DB](./premios-platzi/fun-BD-Normalization.md)

## Creando los modelos Question y Choice

Forma de mantener una organización con las aplicaciones en el archivo settings.

```python
DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

THIRD_APPS = (
    'jazzmin',
)

LOCAL_APPS = (
    'polls',
)


INSTALLED_APPS = THIRD_APPS + DJANGO_APPS  + LOCAL_APPS
```

Al hacer **cambios** en sus modelos o **crear **un nuevo modelo (en models .py).

- Ejecute el comando:

```sh
 python manage .py makemigrations 
```

para crear migraciones para esos cambios

- Ejecute el comando:

```sh
python manage .py migrate
```

para aplicar esos cambios a la base de datos.

> VScode: visualizar las tablas de la base de datos sqlite3, instalar la extension de VScode llamada: SQLite

# 4. Interactive Shell

## La consola interactiva de Django

Otra forma de crear y guardar datos en un modelo sería con:

```python
q  = Question.objects.create(question_text="¿Cuál es el mejor curso de platzi?" , pub_date=timezone.now())
```

de esa forma ya no se necesita hacer el `q.save()` para guardar el dato.

Comando para acceder a la consola interactiva de Django: `python3 manage.py shell`

```python
# Importacion de los modelos
from polls.models import Question, Choice

# Llamado de todos los registros de un modelo
Question.objects.all()

# Creacion de un nuevo registro
q = Question(question_text="¿Cual es el mejor curso de Platzi?", pub_date=timezone.now())

# Guardado del nuevo registro
q.save()
```

 timezone

```python
from django.utils import timezone
```

### Interactura con la consola de django shell

shell

```sh
python manage.py shell
```

Imports

```python
>>> from polls.models import Question, Choice
>>> Question.objects.all()
<QuerySet [<Question: Cual es el mejor curso de Platzi?>]>
>>> from django.utils import timezone
>>> q = Question(question_text="Cual es el mejor curso de Platzi?", pub_date=timezone.now())
>>> q.save()
>>> q
<Question: Cual es el mejor curso de Platzi?>
>>> 
```

## El método __str__

Investigar lo que es shell plus y tratar de usarlo en la app. Con lo que llevamos del curso ya se puede usar perfectamente y sirve para leer documentación de algo que seguramente querrás usar.

> **class datetime.timedelta**
> Una duración que expresa la diferencia entre dos instancias de fecha, hora o datetime con una resolución de microsegundos.

Los “dunder methods” son una manera en la cual nosotros como programadores podemos establecer el comportamiento que tendran nuestros objetos al aplicar sobre ellos diversas built-in functions de Python.

- __str __: definimos el comportamiento que tendrá un objeto A al aplicar ‘str(a)’
- __len __: definimos el comportamiento de len(a)

**Esos son solo algunos, hay muchos más**

fecha con formato especial en el **str** y queda mejor:

```python
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
```

![guia.png](https://static.platzi.com/media/user_upload/guia-1345129a-5c74-47cc-a152-36c40d929343.jpg)

### Shell Django

```python
# second part shell
>>> q
<Question: Cual es el mejor curso de Platzi?>
>>> q.question_text
'Cual es el mejor curso de Platzi?'
>>> q.pub_date
datetime.datetime(2022, 4, 4, 21, 33, 32, 212002, tzinfo=datetime.timezone.utc)

# agregar __str__ a models.py | migrations
	def __str__(self):
        return self.question_text
 
# busqueda
>>> Question.objects.all()
<QuerySet [<Question: Cual es el mejor curso de Platzi?>]>
```



## Filtrando los objetos creados desde la consola interactiva

> *Model.objects.get(condición) --> solo retorna 1 objeto*

**Fecha automatica**

```python
    pub_date = models.DateField(auto_now_add=True)
```

método **get** hay que ser muy especifico, ya que solo trae uno, por el ejemplo estableciendo de filtro la “pk”, para traer datos con cierta condición es mejor usar **filter** y **exclude** para tenerlo más preciso.

Para saber mas sobre las búsquedas en los queries, les dejo la documentación en el apartado que explican esto a [detalle.](https://docs.djangoproject.com/en/4.0/topics/db/queries/#retrieving-specific-objects-with-filters)

> El get comúnmente se usa para buscar por el campo identificador o llave primaria del modelo, porque solo devuelve una coincidencia, para otro tipo de condiciones, se usa filter y exclude como bien comentas.

el get con dia, mes o año:

```python
get(pub_date__year=timezone.now().year)
get(pub_date__year=timezone.now().month)
get(pub_date__year=timezone.now().day)
```

El doble guión bajo “__” en consultar ORM Django sirve para acceder a atributos o métodos de una clase. Por ejemplo

```python
Model.object.get(pudb_date__year=timezon.now)
```

ejemplo de como filtrar las preguntas que hayan sido creadas doce o mas horas antes que el timestamp actual.

```python
from datetime import timedelta

from django.utils import timezone

Questions.objects.filter( pub_time__lte = (timezone.now() - timedelta(hours=12) ) )
```

### Shell django

```python
>>> from polls.models import Question, Choice
>>> Question.objects.all()
<QuerySet [<Question: Cual es el mejor curso de Platzi?>, <Question: Cual es el mejor curso de Platzi?>]>

# importar timezone
>>> from django.utils import timezone

# Crear una pregunta y guardar
>>> Question(question_text="Quien es el mejor profesor de Platzi", pub_date=timezone.now()).save()
>>> Question(question_text="Cual es la mejor Escuela de Platzi", pub_date=timezone.now()).save()
>>> Question.objects.all()
<QuerySet [<Question: Cual es el mejor curso de Platzi?>, <Question: Cual es el mejor curso de Platzi?>, <Question: Quien es el mejor profesor de Platzi>, <Question: Cual es la mejor Escuela de Platzi>]>

# get | utiliza pk para generear los resultados
>>> Question.objects.get(pk=1)
<Question: Cual es el mejor curso de Platzi?>
>>> Question.objects.get(pk=2)
<Question: Cual es el mejor curso de Platzi?>
>>> Question.objects.get(pk=3)
<Question: Quien es el mejor profesor de Platzi>
>>> Question.objects.get(pk=4)
<Question: Cual es la mejor Escuela de Platzi>

# Error al generar el resultado que no existe
>>> Question.objects.get(pk=5)
Traceback (most recent call last):
  File "<console>", line 1, in <module>
  File "/home/hades/.pyenv/versions/premios-platzi/lib/python3.10/site-packages/django/db/models/manager.py", line 85, in manager_method
    return getattr(self.get_queryset(), name)(*args, **kwargs)
  File "/home/hades/.pyenv/versions/premios-platzi/lib/python3.10/site-packages/django/db/models/query.py", line 439, in get
    raise self.model.DoesNotExist(
polls.models.Question.DoesNotExist: Question matching query does not exist.
>>> 
        
# Multiples consultas, solo necesitamos una>>> Question.objects.get(pub_date__year=timezone.now().year)
Traceback (most recent call last):
  File "<console>", line 1, in <module>
  File "/home/hades/.pyenv/versions/premios-platzi/lib/python3.10/site-packages/django/db/models/manager.py", line 85, in manager_method
    return getattr(self.get_queryset(), name)(*args, **kwargs)
  File "/home/hades/.pyenv/versions/premios-platzi/lib/python3.10/site-packages/django/db/models/query.py", line 443, in get
    raise self.model.MultipleObjectsReturned(
polls.models.Question.MultipleObjectsReturned: get() returned more than one Question -- it returned 4!
>>> 
```

[![img](https://www.google.com/s2/favicons?domain=https://static.djangoproject.com/img/icon-touch.e4872c4da341.png)Making queries | Django documentation | Django](https://docs.djangoproject.com/en/3.2/topics/db/queries/#field-lookups-intro)

## El método filter

campos de búsquedas que encontré en la documentación:
|
**__gt** = Mayor que
**__gte** = Mayor o igual que
**__lt** = Menor que
**__lte** = Menos o igual que
**__startswith** = Empieza con
**__endswith** = Termina con
|

### Shell django

```python
>>> Question.objects.filter(pk=1)
<QuerySet [<Question: Cual es el mejor curso de Platzi?>]>
>>> Question.objects.filter(pk=2)
<QuerySet [<Question: Cual es el mejor curso de Platzi?>]>
>>> Question.objects.filter(pk=3)
<QuerySet [<Question: Quien es el mejor profesor de Platzi>]>
>>> Question.objects.filter(pk=4)
<QuerySet [<Question: Cual es la mejor Escuela de Platzi>]>
>>> Question.objects.filter(pk=5)
<QuerySet []>
>>> 

# Consultas 
>>> Question.objects.filter(question_text__startswith="Cual")
<QuerySet [<Question: Cual es el mejor curso de Platzi?>, <Question: Cual es el mejor curso de Platzi?>, <Question: Cual es la mejor Escuela de Platzi>]>
>>> 

# consultas con filter | fechas
>>> Question.objects.filter(pub_date__year=timezone.now().year)
<QuerySet [<Question: Cual es el mejor curso de Platzi?>, <Question: Cual es el mejor curso de Platzi?>, <Question: Quien es el mejor profesor de Platzi>, <Question: Cual es la mejor Escuela de Platzi>]>
>>> 

```

[método para la búsqueda](https://docs.djangoproject.com/en/4.0/ref/models/querysets/#field-lookups)

## Accediendo al conjunto de respuestas

Otra opción sería asignar un nombre con el que te sea fácil identificar la relación inversa, usando related_name en el atributo question en el modelo Choice.

question= models.ForeignKey(Question, on_delete=models.CASCADE, related_name=‘question_choices’)

De esa forma estaríamos especificando el nombre con el que queremos llamar a la relación inversa y podríamos usar:
q.question_choices.all()
en lugar de q.choice_set.all()

Si nosotros no especificamos el nombre de la relación inversa usando related_name Django crea uno automáticamente usando el nombre del modelo con el sufijo _set. (choice_set)

> Es un poco peculiar que Django en ciertas cosas use el doble underscore (__) en vez del dot notation pero básicamente tiene la misma función.

Cuando una clase/tabla `Choice` tiene una llave foranea hacia una clase/tabla `Question`, existen 2 formas de crear las “choices” de una “question” especifica.
.

1. La primera forma es crear la choice desde su clase, referenciando referenciando el objeto exacto de la question a la que pertenece. Es decir:

   ```Python
   #Primero encontramos el objeto de la question a la que le queremos añadir la choice.
   my_question = Question.objects.get(pk=1)
   
   #Luego le pasamos ese objeto a la choice.
   my_choice = Choice(
   	question_id = my_question,
   	choice_text = "text of my choice"
   )
   #OJO: Como dice un compañero, el argumento "votes" no es necesario pasarlo pues ya 	definimos en el modelo de Choice que tendrá un valor predeterminado de 0
   
   my_choice.save() # Si lo hacemos con esta tecnica es necesario guardar para que los 	cambios se hagan en la db.
   ```

2. La segunda forma es la que enseñó el profesor, en teoria consiste en que django tiene una API que nos ayuda a encontrar todas las clases que tienen una relación por llave foranea hacia nuestra clase/tabla X (Es decir, en este caso nosotros podemos acceder a todas las clases conectadas por llave foranea a nuestra clase `Question` , que en este caso solo tenemos la clase Choice pero podrían ser más)

   He creado una clase igual a Choice pero llamada `OptionalChoice` y puedo crear sus objetos de la misma forma en la que el profesor lo hace en el video con la clase Choice, así:

   ```Python
   my_question = Question.objects.get(pk=1) #Objeto de la question
   my_question.optionalquestion_set.create(choice_text = "text of my optional choice")
   #Recordar que con esta forma de hacer las cosas no es necesario usar el método save().
   ```

- Filtrando las Choice que tiene como Question(FK) al año actual:

> Choice.objects.filter(question__pub_date__year=timezone.now().year)

### shell django

Respuestas 

```python
>>> Question.objects.filter(pub_date__year=timezone.now().year)
<QuerySet [<Question: Cual es el mejor curso de Platzi?>, <Question: Cual es el mejor curso de Platzi?>, <Question: Quien es el mejor profesor de Platzi>, <Question: Cual es la mejor Escuela de Platzi>]>
>>> 

# Respusta a las interrogantes
>>> q.choice_set.create(choice_text="Curso Basico de Python", votes=0)
<Choice: Curso Basico de Python>
>>> q.choice_set.create(choice_text="Curso de Fundamentos de Ingenieria de Software", votes=0)
<Choice: Curso de Fundamentos de Ingenieria de Software>
>>> q.choice_set.create(choice_text="Curso de Elixir", votes=0)
<Choice: Curso de Elixir>

 # Mejor curso
>>> q.choice_set.all()
<QuerySet [<Choice: Curso Basico de Python>, <Choice: Curso de Fundamentos de Ingenieria de Software>, <Choice: Curso de Elixir>]>

# respuestas
>>> q.choice_set.all()
<QuerySet [<Choice: Curso Basico de Python>, <Choice: Curso de Fundamentos de Ingenieria de Software>, <Choice: Curso de Elixir>]>

# Conteo de respuestas
>>> q.choice_set.count()
3

# Fecha de publicacion en la fecha actual
<QuerySet [<Choice: Curso Basico de Python>, <Choice: Curso de Fundamentos de Ingenieria de Software>, <Choice: Curso de Elixir>]>
```



# 5. Django Admin

## El administrador de Django

Question las Choice agregar el siguiente código en el archivo admin y quitando el registro anterior.

```python
class ChoicesInline(admin.StackedInline):
    model = Choice
    can_delete = False
    verbose_name_plural = 'choices'

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    inlines = (ChoicesInline,)
```

> Cuando se crea un usuario usando el admin de Django es importante marcar la opcion is_staff para que pueda ingresar también al admin web

> Para visualizar el administrador de Django en español, tendrían que modificar en el archivo settings la variable LANGUAGE_CODE = 'es-mx’
> En mi caso lo estoy asignando al español de México pero podrían ponerlo al que ustedes requieran.

Para registrar uno o mas modelos puedes pasarlos dentro de una lista:

```python
from django.contrib import admin
from .models import Question, Choice

admin.site.register([Question, Choice])
```

The way we set a scalabable way to access the queries and handling the data in a no-code way

```bash
python manage.py createsuperuser
Username (leave blank to use 'root'): aprendefelipe
Email address: felipe.salda@hotmail.com
Password: 
Password (again): 
Superuser created successfully.
```

First we have to set the availability of the model for the admin at polls/admin.py

```python
from django.contrib import admin
from .models import Question, Choice

admin.site.register(Question) #this function set the model available for the admin
admin.site.register(Choice)
```

at /admin
![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-6cfe1695-882b-4807-b208-cf802bee8a08.jpg)
![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-c92983b3-6bf4-4807-a1c9-bbd5d2213c2b.jpg)

Here we can access to our questions and take actions like create, delete and so on
![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-7891f38a-6b29-474c-ae94-93c4f8a66329.jpg)

it’s impresive that the choices are related to the question as we set and also we can change it or create and relate choices to other questions
![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-b7cea181-4d0e-45d0-8f1d-fb809df2693f.jpg)

Users management
![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-fdadc74a-64ec-403e-8ad5-21e20b9c0bb5.jpg)

Groups Management
![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-498d4820-7fc8-46ce-aeba-2561d36b9fda.jpg)

# 6. Views

## ¿Qué son las views o vistas?

### Diferencia entre MVC y MTV 😮

Muy similar a MVC (Modelo, vista, controlador) donde:
Modelo - Hace referencia a todo lo que tiene que ver con bases de datos.

------

Vista - Con la parte visual.

------

Controlador - Con toda la parte lógica.

------

En Django siendo MTV 😃

**Modelo** - Hace referencia a todo lo que tiene que ver con bases de datos (En este caso Django hace demasiada alusión a su nombre donde las bases de datos quedan implícitas y manejamos todo con el ORM)

------

**Template** - En este caso no debemos confundir el View del MTV con el del MVC puesto en que en Django no hace referencia a lo visual, **template** si hace alusión con la parte visual de las Web Apps con Django.

------

**View** - Todo lo relacionado con la lógica es aquí donde entra el tema de las vistas genéricas y demás (Que si solo nos especializamos en el back con Django es lo que mas utilizaremos)

MTV => MVC:

- Views + Urls => Controller
- Templates => View
- Models => Model

![Django.png](https://static.platzi.com/media/user_upload/Django-73d3c797-4724-493a-b32b-c666ba1cd545.jpg)

DRF **(Django Rest Framework)** Es la librería externa de Django más usada para la creación de **Rest API’s**, la cual nos permite a través de protocolos y definiciones diseñar e integrar software de aplicaciones; usando la arquitectura **REST** para transmitir los datos a través de **JSON** con la petición (request) y la respuesta (response) entre cliente y servidor.

 La adaptación de MVC en Django es MTV, y volviendo las Views cómo si fuera el controlador de Vista (front-end) y motor (back-end).

![mtv.png](https://static.platzi.com/media/user_upload/mtv-eab716f1-effd-49ea-9b21-85494b9b6da2.jpg)

![views.png](https://static.platzi.com/media/user_upload/views-6d3f3291-a2e5-4cf7-bf8c-5224975ee3fb.jpg)

## Creando vistas para la aplicación

Creación de views en polls/views-py

```python
def detail(request, question_id):
    return HttpResponse(f"Estas viendo la pregunta número {question_id}")
```

Creación de re-dirección en polls/url-py

```python
urlpatterns = [
    path('', views.index, name='index'),
    #ex: /polls/5/
    path('<int:question_id>/', views.detail, name='index'),
]
```

El ejemplo es básico, pero tener el concepto claro es importante para profundizar en el manejo de variables e importación de datos.

> Las **views** pueden definirse con:
> \- **Functions:** Functions based views
> \- **Class:** Generic views
>
> Definir **views** con Generic views es mucho mas rápido cuando debemos definir vista compuestas por **Templates, Querysets, Contexts, Models, Forms.** Entre otros según el modelo de negocio.

**Resumen de como trabajan las Vista (Views) al recibir un request por el navegador para luego entregar una respuesta:**
.
1- Se manda un request por la barra de URL en el navegador.
2 - Se recibe en el archivo **urls py** y según la URL envidada se determina cual es el view correspondiente en el archivo **views py**.
3 - Ser recibe el request en **views py** en la función o clase correspondiente y se retorna una respuesta.

Quedé con muchas preguntas respecto a las url, si desean profundizar en el tema aquí les comparto los recursosl. Saludos!!

[How Django processes a request](https://docs.djangoproject.com/en/4.0/topics/http/urls/#how-django-processes-a-request)
[Using regular expressions](https://docs.djangoproject.com/en/4.0/topics/http/urls/#using-regular-expressions)
[Including other URLconfs](https://docs.djangoproject.com/en/4.0/topics/http/urls/#including-other-urlconfs)
[Captured parameters](https://docs.djangoproject.com/en/4.0/topics/http/urls/#captured-parameters)

> Problema con los import en la parte de urls, al importar las views como Facundo no me andaba el server, el usa from . import views, a mi no me funciona, así que reemplace el ’ . ’ por ‘polls’ y me funciona. Así que si alguien tiene ese problema ya sabe:
> from polls import views.

 polls/views.py we will create or functions views for the app

```python
from django.shortcuts import render
from django.http import HttpResponse

#function based view
#every function works with requests and http response 
def index(request):
    """our index page view"""
    return HttpResponse("Página Principal de Premios Platzi App😎")

def detail(request, question_id):
    """details of every question"""
    return HttpResponse(f'Estás viendo la pregunta {question_id}')

def results(request, question_id):
    """details of every question result"""
    return HttpResponse(f'Estás viendo los resultados de la pregunta {question_id}')

def vote(request, question_id):
    """Functions for voting confirmation"""
    return HttpResponse(f'Estás votando a la pregunta {question_id}')
```

at polls/urls.py we set our paths for the functions

```python
from django.urls import path

from . import views #from here import...

urlpatterns = [
    #ex: /polls/
    path("", views.index, name="index"),
    #ex: /polls/5/
    path("<int:question_id>/", views.detail, name="index"),
    #ex: /polls/5/details
    path("<int:question_id>/", views.detail, name="index"),
    #ex: /polls/5/results
    path("<int:question_id>/results/", views.results, name="index"),
    #ex: /polls/5/vote
    path("<int:question_id>/vote/", views.vote, name="index"),
]

# "< >/ is the way we pass parameters and variables through the url we are setting"
```

Some examples

![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-4d15b7ce-71b6-4434-8ca6-373dd51ae263.jpg)

![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-0073c902-eb76-42e3-981f-5833ef5ad008.jpg)

Notices that we are not connected to the current question database yet, that’s why it doesn’t rise error where a non existing question is called at the url.

## Templates de Django

 los shortcuts de Django es porque hay que modificar el settings.json. Deben agregar:

```json
"files.associations": {
        "**/*.html": "html",
        "**/templates/**/*.html": "django-html",
        "**/templates/**/*": "django-txt",
        "**/requirements{/**,*}.{txt,in}": "pip-requirements"
    },
```

Si les deja de funcionar los shortcuts de HTML deben agregar también:

```json
 "emmet.includeLanguages": {"django-html": "html"}
```

> Django tiene un sistema de templates llamado el Django Template System, en el cual podemos incluir código de Python en un documento HTML.

> > Para los snippets de Django utilizar la extensión que se llama Djaneiro en VScode

## Creando el template del home

> El motor de templates usa `{% %}` para todo lo que son bloques, ya sean operaciones como condicionales, ciclos o bloques personalizados que se utilizan para heredar datos entre templates y `{{ }}` se usa para las variables.

El render es una función importada de Django. Necesita tres parámetros

- El *request*
- La ubicación de la template correlacionada. En la ruta se omite la carpeta */template/* ya que Django junta todas las carpetas templates de todas las aplicaciones de nuestro proyecto y las hace una sola
- Un objeto con la o las variables que le estaremos pasando a nuestra **template**

El atributo **href** en la etiqueta **<a>**
Su valor va de esta manera: Sin el ** polls/ **

```jinja2
<li><a href="{{ question.id }}">{{ question.question_text }}</a> </li>
```

Porque ya estamos en path ** polls/ **

Sino crearía conflictos el path de la functions ** detail() ** al darle click al hiper vinculo de las pregunta renderisadas en este template.

templates/polls/index.html for the index template

```html
{% if latest_question_list %}
    <ul>
        {% for question in latest_question_list %}
            <li><a href="/polls/{{ question.id }}">{{ question.question_text }}</a> </li>
        {% endfor %}
    </ul>
{% else %}
    <p>No polls are available 😓.</p>
{% endif %}
```


Also, we need to update our polls/views.py to connect with the template.

```python
from django.shortcuts import render
from django.http import HttpResponse
from .models import Question

#function based view
#every function works with requests and http response 
def index(request):
    """our index page view"""
    latest_question_list = Question.objects.all()
    return render(request, "polls/index.html",{
        "latest_question_list":latest_question_list
        })

def detail(request, question_id):
    """details of the question"""
    return HttpResponse(f'Estás viendo la pregunta {question_id}')

def results(request, question_id):
    """details of every question result"""
    return HttpResponse(f'Estás viendo los resultados de la pregunta {question_id}')

def vote(request, question_id):
    """Functions for voting confirmation"""
    return HttpResponse(f'Estás votando a la pregunta {question_id}')
```


Notices two important things:

- latest_question_list is a query as we learned at the interactive shell classes
- render use the same django structure to find the template, that’s the importances of our order.

## Elevando el error 404

Personalizar Error 404 puedes hacer lo siguiente:

![404.png](https://static.platzi.com/media/user_upload/404-9d000850-799e-47d6-9afa-2b498fb45420.jpg)

En settings,py debes modificar lo siguiente:

```python
DEBUG = False
ALLOWED_HOSTS = ['*']
```

Y el archivo debe ir dentro de polls/templates/404,html.

Este es un tema muy interesante acerca de los shortcut functions de Django, en este documento cortico encontrarán toda la información valiosa para entender el funcionamiento más a profundidad, espero sea de ayuda.

Allí se tratan 4 puntos importantes:

- render()
- redirect()
- get_object_or_404()
- get_list_or_404()

respuestas simplemente se pone una condicion:

```jinja2
<h1>{{question.question_text}}</h1>
<ul>
    {% if question.question_choices.all %}
        {% for choice in question.question_choices.all %}
            <li>{{choice.choice_text}}</li>
        {% endfor %}
    {% else %}
        <p>No choices for this question</p>
    {% endif %}
    
</ul> 
```

[Django shortcut functions](https://docs.djangoproject.com/en/4.0/topics/http/shortcuts/)

[Esta es una web de gatos para consultar los errores http ♥](https://http.cat/)

## Utilizando la etiqueta url para evitar el hard coding

> **app_name** ya que sí la utiliza internamente.
>
> ```python
> app_name = 'polls'
> ```

at index.html we are hard coding at the href link and this can be fuckd up if we change the path or the name of the app.

*How to avoid it → modularizing our code*

First, we have to set an app name to pass into the href of index.html

urls,py

```python
from django.urls import path
from . import views #from here import...

**app_name = "polls"**
urlpatterns = [
    #ex: /polls/
    path("", views.index, name="index"),
    #ex: /polls/5/
    **path("<int:question_id>/details/pruebadecambiopademostrarquefuncionalaurltag", views.detail, name="detail"),**
    #ex: /polls/5/results
    path("<int:question_id>/results/", views.results, name="results"),
    #ex: /polls/5/vote
    path("<int:question_id>/vote/", views.vote, name="votes"),    
]

# "< >/ is the way we pass parameters and variables through the url we are setting"
```

at index,html

```html
{% if latest_question_list %}
    <ul>
        {% for question in latest_question_list %}
            <li><a href="{% url 'polls:detail' question.id %}">{{ question.question_text }}</a></li>
        {% endfor %}
    </ul>
{% else %}
    <p>No polls are available 😓.</p>
{% endif %}
```

![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-a97fdc1e-68ad-4beb-be6d-b741d49d6e61.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://static.djangoproject.com/img/icon-touch.e4872c4da341.png)Built-in template tags and filters | Django documentation | Django](https://docs.djangoproject.com/en/4.0/ref/templates/builtins/)

[![img](https://www.google.com/s2/favicons?domain=https://static.djangoproject.com/img/icon-touch.e4872c4da341.png)The Django template language | Django documentation | Django](https://docs.djangoproject.com/en/4.0/ref/templates/language/)

# 7. Forms

## Formularios: lo básico

Para validar la etiqueta **<input>**

Y evitar que al presinar el botón de Votar, envíe el formulario sin haber seleccionado alguna de las opciones.

HTML5 nos facilita esta validación agreagando **required**
a la etiqueta **<input>**

Y ya tendremos validado el formulario. 😉

```html
<input
                    type="radio"
                    name="choice"
                    id="choice{{ forloop.counter }}"
                    value="choice.id"
                    required
>
```

> **CSRF** = Cross-site request forgery (falsificación de peticiones inter-sitio).
>
> CSRF: el Cross Site Request Forgery (CSRF o XSRF) es un tipo de ataque que se suele usar para estafas por Internet. Los delincuentes se apoderan de una sesión autorizada por el usuario (session riding) para realizar actos dañinos. El proceso se lleva a cabo mediante solicitudes HTTP.

```jinja2
<!-- comentar -->
{% comment %} 
😎
{% endcomment %}
```

## Creando la vista vote

`detail.html`

```jinja2
<form action="{% url 'polls:vote' question.id %}" method="post">
{% csrf_token %}
<fieldset>
  <legend>
    <h1>{{ question.question_text}}</h1>
  </legend>
  
  {% if error_message %}
    <p><strong>{{ error_message }}</strong></p>
  {% endif %}
  
  {% for choice in question.choice_set.all %}
    <input 
      type="radio"
      name="choice"
      id="choice{{ forloop.counter }}"
      value="{{choice.id}}"
    >
    <label for="choice{{ forloop.counter }}">
      {{ choice.choice_text}}
    </label>
    <br>
  {% endfor %}
</fieldset>
<input type="submit" value="Voter">
</form>

```

urls.py -> function -> vote 

```python
def results(request, question_id):
  return HttpResponse(f"Estas viendo los resultados de la pregunta {question_id}")

# Vote
def vote(request, question_id):
  question = get_object_or_404(Question, pk=question_id)
  try:
    selected_choice = question.choice_set.get(pk=request.POST["choice"])
  except (KeyError, Choice.DoesNotExist):
    return render(request, "polls/detail.html", {
      "question": question,
      "error_message": "No elegiste una respuesta"
    })
  else:
    selected_choice.votes += 1
    selected_choice.save()
    return HttpResponseRedirect(reverse("polls:results", args=(question.id,)))
```



## Creando la vista results

`result.html`

```jinja2
<h1>{{ question.question_text }}</h1>
<ul>
  {% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }} -- {{ choice.votes }} vote{{ choice.votes | pluralize }}</li>
  {% endfor %}
</ul>
<a href="{% url 'polls:detail' question.id %}">Te gustaria votar de nuevo?</a>
```

`views.py`

```python
def results(request, question_id):
  question = get_object_or_404(Question, pk=question_id)
  return render(request, "polls/results.html", { 
    "question": question
  })
```



# 8. Generic Views

## Generic Views

**LoginView**

**Se usa con:** `from django.contrib.auth.views import LoginView`

Se usa para desplegar el login form y manejar toda la accion de logueo

![loginView.png](https://static.platzi.com/media/user_upload/loginView-7d4321cd-9bae-49ab-8423-c9e9eabce41f.jpg)

> [ccbv.co.uk](https://ccbv.co.uk/)  ver detalladamente las class based views, les muestran jerarquías y herencias para entender como operan las mismas.

[Built-in class-based views API](https://docs.djangoproject.com/en/4.0/ref/class-based-views/)

**UpdateView**
Vista para actualizar un objecto, con una respuesta renderizada por un template.
Los atributos más importantes son:

```python
model, success_url, template_name.
```

Los métodos más importantes son:

```python
get, post, put, as_view
```

> **DetailView:**
>
> Representar una vista de “detalle” de un objeto.
>
> La importamos en las **view** con:
>
> ```python
> from django.views.generic import DetailView
> ```
>
> Con lo aprendido hasta ahora. Podemos sacar provecho de esta **Generic View**:
>
> Los atributos:
> **model = modelo_a_mostrar**
> **template_name=template_a_mostrar_el_detalle**
>
> Podemos defininir estos atributos y ya no deberíamos indicarlos en el **return**

`PasswordChangeView` es utilizada para cambiar la contraseña, por defecto trae la template `registration/password_change_form.html`

> **LoginView**
> Nos muestra el formulario de inicio de sesión (Username, Password y submit).
>
> **UpdateView**
> Vista para actualizar un objeto, con una respuesta renderizada por una plantilla.

LogoutView:

Esta vista hace logout al usuario además de mostrarle el mensaje “You’re logged out”.

Posee varios atributos como:

- content_type
- next_page
- response_class

Así como métodos:

- get()
- get_context_data()
- get_next_page()

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Django Class-Based-View Inspector -- Classy CBV](http://ccbv.co.uk/)

## Implementando generic views en la aplicación

OOP podemos redicir aun mas el problema de DRY haciendo uso de la herencia

```python
class DetailView(generic.DetailView):
    model = Question
    template_name = "polls/details.html"


class ResultView(DetailView):
    template_name = "polls/results.html"
```

¿Cuando usar **Generic views** ?

Usar solo si podemos seguir el patrón:

- Cargar de la base de datos
- Generar template
- Mostrarlo

Si en la lógica debemos implementar cosas más complejas hay que utilizar **Function views**

importar de manera individual cada generic puedes hacer lo siguiente:

```python
from django.views.generic import ListView, DetailView
```

**Si se puede:**

- Generic views.

  Si podemos seguir el patrón:

  - cargar datos de la base de datos
  - Generar template
  - Mostrar template

**Si no se pude:**

- Functions based views.
  Escaparse del patrón anterior. Si es mas complejo, como mostrar 2 formularios en la misma página

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Django Class-Based-View Inspector -- Classy CBV](http://ccbv.co.uk/)

# 9.Conclusiones

## Aprendiste mucho, pero, ¿estás listo para pasar al siguiente nivel?

Resumen de curso:

- Sabes que es django y su posición en los framework
- Sabes instalar django, App Factory, crear proyecto y leerlo
- ORM (Replicar la estructura de una DB relacional)
- Usar Django shell & admin
- Formularios y Htto(POST)
- Function-based views y class-based views

No pares de Aprender!